//(function () {
const Company = (function () {
  const _registerUserCreateListeners = [];
  const _registerUserUpdateListeners = [];
  const _registerNoSpaceNotifye = [];

  class Company {
    constructor(name, maxLenth) {
      this.name = name;
      this.maxLenth = maxLenth;
      this.users = [];

    }
    static createSuperAdmin(company) {
      if (company.users[0]) {
        return 'Has already been created';
      }
      const admin = new User('Harry', 'Potter', true);
      admin.company = company;
      company.users.push(admin);
      return admin;
    }
    get curSize() {
      return this.users.length;
    }

    getUser(id) {
      let foundUser = this.users.find(user => user.id === id)
      return foundUser;
    }
    registerUserCreateCallback(...callback) {
      registerUserCreateListeners.push(...callback);
    }

    registerUserUpdateCallback(...callback) {
      registerUserUpdateListeners.push(...callback);
    }
    registerNoSpaceNotifye(...callback) {
      registerNoSpaceNotifye.push(...callback);
    }

  }


  class User {
    constructor(name, lastName, isAdmin) {
      this.name = name;
      this.lastName = lastName;
      this.id = Math.random().toString(36).slice(2)

      if (isAdmin) {
        this._token = 'secret token';
        this.password = prompt('enter password')

        this.createUser = function (name, lastName) {
          let company = this.company;
          if (company.users.length >= company.maxLenth) {
            return 'maximum number of users'
          }
          if (company.users.length === company.maxLenth) {
           _registerNoSpaceNotifye.forEach(cb => cb("last user!"))
          }
          const newUser = new User(name, lastName)
          const password = prompt('enter password');

          company.users.push(newUser);
          if (password != this.password) {
            throw new Error('Error')
          }
          if (this._token != 'secret token') {
            throw new Error('Error')
          }

         _registerUserCreateListeners.forEach(cb => cb(newUser))
          return newUser;
        }
        this.delete = function (id) {
          let company = this.company;
          company._registerUserUpdateListeners.forEach(cb => cb(id))
          return company.users.filter(i => i.id === id);

        }
      }
    }

    get userName() {
      return `${this.name}`;
    }

    set userName(value) {
      this.name = value;
      _registerUserUpdateListeners.forEach(cb => cb(this))
    }


    get userLastName() {
      return `${this.lastName}`;
    }

    set userLastName(value) {
      [this.lastName] = value;
      _registerUserUpdateListeners.forEach(cb => cb(this))
    }
  }
  return Company;
})()



let company = new Company('Y', 3);
console.log(company)